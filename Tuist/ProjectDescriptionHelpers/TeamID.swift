import Foundation

public struct TeamID: ExpressibleByStringInterpolation, CustomStringConvertible {
    public static let freedom: Self = "DSKL7YS6PW"

    public let description: String

    public init(stringLiteral value: String) {
        description = value
    }
}
