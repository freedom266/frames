import struct ProjectDescription.Configuration

public struct AppSetup {
    public static var current: Self {
        .init(environment: .current, configuration: .current)
    }

    public let environment: Environment
    public let configuration: Configuration
    public let moduleBundleIDPrefix = "cz.freedom.frames"

    public var bundleID: String {
        switch (configuration, environment) {
        case (.debug, _):
            moduleBundleIDPrefix + ".debug"
        case (.beta, .stage):
            moduleBundleIDPrefix + ".beta.stage"
        case (.release, .production):
            moduleBundleIDPrefix + ".release"
        case (.release, _), (.beta, .development), (.beta, .production):
            fatalError("Cannot make release configuration with \(environment.appNameValue) environment")
        }
    }

    public var appNameValue: String {
        if configuration == .release, environment == .production {
            return ""
        }

        return environment.appNameValue
    }

    public var projectConfigurations: [ProjectDescription.Configuration] {
        switch configuration {
        case .debug:
            return [.debug(name: "Debug")]
        case .beta, .release:
            return [.release(name: "Release")]
        }
    }

    public var teamID: TeamID {
        switch configuration {
        case .debug, .beta:
            return .freedom
        case .release:
            return .freedom
        }
    }

    public var codeSignStyle: String {
        switch configuration {
        case .debug:
            return "Automatic"
        case .beta, .release:
            return "Manual"
        }
    }
}

extension AppSetup: CustomStringConvertible {
    public var description: String { "\(configuration)/\(environment)" }
}
