import ProjectDescription

private let targetName = "FramesUI"
private let bundleID = "\(AppSetup.current.moduleBundleIDPrefix).ui"

public let designSystem = Target(
    name: targetName,
    platform: .iOS,
    product: .framework,
    bundleId: bundleID,
    infoPlist: .default,
    sources: "\(targetName)/Sources/**",
    entitlements: nil,
    dependencies: [
        .target(name: resources.name),
        .target(name: core.name)
    ]
)

public let designSystemTests = Target(
    name: targetName + "_Tests",
    platform: .iOS,
    product: .unitTests,
    bundleId: bundleID + ".unittests",
    sources: "\(targetName)/Tests/**",
    dependencies: [
        .target(designSystem),
        .target(testing)
    ]
)
