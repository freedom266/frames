import ProjectDescription

private let targetName = "FramesCore"
private let bundleID = "\(AppSetup.current.moduleBundleIDPrefix).core"

public let core = Target(
    name: targetName,
    platform: .iOS,
    product: .framework,
    bundleId: bundleID,
    infoPlist: .default,
    sources: "\(targetName)/Sources/**",
    resources: [
        "\(targetName)/Environment/\(Environment.current)/**"
    ],
    entitlements: nil,
    scripts: [
        .swiftlint
    ],
    dependencies: [
        .external(name: "ACKategories"),
		.external(name: "SwiftyBeaver"),
		.target(name: resources.name)
    ]
)
