import ProjectDescription

public let example = Feature(
    name: "Example",
    dependencies: [
        .target(name: core.name),
        .target(name: designSystem.name),
        .target(name: resources.name)
    ],
    hasTests: true,
	hasTesting: true
)
