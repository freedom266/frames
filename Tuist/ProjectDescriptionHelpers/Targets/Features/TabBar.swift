import ProjectDescription

public let tabBar = Feature(
	name: "TabBar",
	dependencies: [
		.target(name: core.name),
		.target(name: designSystem.name),
		.target(name: resources.name),
		.target(example)
	],
	hasTests: false
)
