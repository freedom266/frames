//
//  Edit.swift
//  ProjectDescriptionHelpers
//
//  Created by Martin Svoboda on 06.04.2024.
//

import Foundation

public let edit = Feature(
	name: "Edit",
	dependencies: [
		.target(name: core.name),
		.target(name: designSystem.name),
		.target(name: resources.name)
	],
	hasTests: true,
	hasTesting: true
)
