import ProjectDescription
import ProjectDescriptionHelpers

let dependencies = Dependencies(
    swiftPackageManager: .init(
        [
            .remote(
                url: "https://github.com/pointfreeco/swift-snapshot-testing",
                requirement: .upToNextMajor(from: "1.10.0")
            ),
            .remote(
                url: "https://github.com/AckeeCZ/ACKategories",
                requirement: .upToNextMajor(from: "6.12.0")
            ),
			.remote(
				url: "https://github.com/SwiftyBeaver/SwiftyBeaver",
				requirement: .upToNextMajor(from: "2.0.0")
			)
        ],
        baseSettings: .settings(configurations: AppSetup.current.projectConfigurations)
    ),
    platforms: [.iOS]
)
