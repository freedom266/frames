//
//  TextStyles.swift
//  Frames
//
//  Created by Martin Svoboda on 13.02.2024.
//  Copyright © 2024 Freedom, s.r.o. All rights reserved.
//

import SwiftUI
import ACKategories

public extension View {
	
	// MARK: Headlines
	
	/// Headline 1
	///
	/// * Font: `SFPro.semibold`
	/// * Size: `28`
	/// * Line height: `33`
	func headline1() -> some View {
		font(SFPro.semibold.font(size: 28), lineHeight: 33, textStyle: .title)
	}
	
	/// Headline 2
	///
	/// * Font: `SFPro.semiBold`
	/// * Size: `20`
	/// * Line height: `24`
	func headline2() -> some View {
		font(SFPro.semibold.font(size: 20), lineHeight: 24, textStyle: .title2)
	}
	
	// MARK: Texts
	
	/// Text Small
	///
	/// * Font: `SFPro.regular`
	/// * Size: `12`
	/// * Line height: `16`
	func textSmall() -> some View {
		font(SFPro.regular.font(size: 12), lineHeight: 16, textStyle: .body)
	}
	
	/// Text Medium
	///
	/// * Font: `SFPro.regular`
	/// * Size: `14`
	/// * Line height: `18`
	func textMedium() -> some View {
		font(SFPro.regular.font(size: 14), lineHeight: 18, textStyle: .body)
	}
	
	/// Text Large
	///
	/// * Font: `SFPro.regular`
	/// * Size: `16`
	/// * Line height: `20`
	func textLarge() -> some View {
		font(SFPro.regular.font(size: 16), lineHeight: 20, textStyle: .body)
	}
	
	// MARK: Labels
	
	/// Label Mini
	///
	/// * Font: `SFPro.medium`
	/// * Size: `10`
	/// * Line height: `12`
	func labelMini() -> some View {
		font(SFPro.medium.font(size: 10), lineHeight: 12, textStyle: .body)
	}
	
	/// Label Small
	///
	/// * Font: `SFPro.medium`
	/// * Size: `12`
	/// * Line height: `16`
	func labelSmall() -> some View {
		font(SFPro.medium.font(size: 12), lineHeight: 16, textStyle: .body)
	}
	
	// MARK: Buttons
	
	/// Button Large
	///
	/// * Font: `SFPro.semiBold`
	/// * Size: `16`
	/// * Line height: `20`
	func buttonLarge() -> some View {
		font(SFPro.semibold.font(size: 16), lineHeight: 20, textStyle: .body)
	}
}

#if DEBUG
#Preview {
	VStack(spacing: 20) {
		Text("headline1").headline1()
		Text("headline2").headline2()
		
		Divider()
		
		Text("textSmall").textSmall()
		Text("textMedium").textMedium()
		Text("textLarge").textLarge()
		
		Divider()
		
		Text("labelMini").labelMini()
		Text("labelSmall").labelSmall()
		
		Divider()
		
		Text("buttonLarge").buttonLarge()
	}
	.padding()
	.scrollable()
}
#endif
