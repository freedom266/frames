//
//  SFProFont.swift
//  Frames
//
//  Created by Martin Svoboda on 13.02.2024.
//  Copyright © 2024 Freedom, s.r.o. All rights reserved.
//

import SwiftUI
import FramesResources

public enum SFPro {
	case light      // weight 300
	case regular    // weight 400
	case medium     // weight 500
	case semibold   // weight 600
	case bold       // weight 700

	var font: FramesResourcesFontConvertible {
		switch self {
		case .light: FramesResourcesFontFamily.SFPro.light
		case .regular: FramesResourcesFontFamily.SFPro.regular
		case .medium: FramesResourcesFontFamily.SFPro.medium
		case .semibold: FramesResourcesFontFamily.SFPro.semibold
		case .bold: FramesResourcesFontFamily.SFPro.bold
		}
	}

	public func font(size: CGFloat) -> UIFont {
		font.font(size: size)
	}
}
