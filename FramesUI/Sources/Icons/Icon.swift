//
//  Icon.swift
//  Frames
//
//  Created by Martin Svoboda on 13.02.2024.
//  Copyright © 2024 Freedom, s.r.o. All rights reserved.
//

import SwiftUI

public struct Icon: View {
	private let name: String
	
	public var body: some View {
		image
	}
}

private extension Icon {
	var image: Image { .init(systemName: name) }
}

extension Icon: ExpressibleByStringLiteral {
	public init(stringLiteral value: StaticString) {
		self.name = "\(value)"
	}
}

public extension Icon {
	func size(_ size: CGFloat) -> some View {
		image
			.font(.system(size: size))
	}
}

public extension Icon {
	/// checkmark.circle.fill
	static var checkmark: Icon = "checkmark.circle.fill"
	/// xmark.circle.fill
	static var xmark: Icon = "xmark.circle.fill"
	/// doc.on.doc
	static var copy: Icon = "doc.on.doc"
	/// plus
	static var plus: Icon = "plus"
}

#Preview {
	VStack(spacing: 40) {
		Icon.checkmark
			.size(25)
			.foregroundColor(.basicGreen)
		
		Icon.xmark
			.size(25)
			.foregroundColor(.basicRed)
		
		Icon.copy
			.size(25)
			.foregroundColor(.basicBlue)
		
		Icon.plus
			.size(25)
			.foregroundColor(.basicBlack)
	}
	.frame(maxWidth: 300, maxHeight: 300)
	.padding()
	.backgroundColor(.backgroundsPrimary)
}
