//
//  View+Extensions.swift
//  Frames
//
//  Created by Martin Svoboda on 06.08.2023.
//  Copyright © 2023 Freedom, s.r.o. All rights reserved.
//

import SwiftUI

public extension View {
    @available(iOSApplicationExtension, unavailable)
    func endEditingOnTap() -> some View {
        onTapGesture {
            UIApplication.shared.sendAction(#selector(UIResponder.resignFirstResponder), to: nil, from: nil, for: nil)
        }
    }
}

public extension View {
	func frame(size: CGSize, alignment: Alignment = .center) -> some View {
		frame(width: size.width, height: size.height, alignment: alignment)
	}
	
	func frame(size: CGFloat, alignment: Alignment = .center) -> some View {
		frame(size: .init(width: size, height: size))
	}
}
