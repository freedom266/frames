//
//  AddVideoButton.swift
//  FramesUI
//
//  Created by Martin Svoboda on 07.04.2024.
//  Copyright © 2024 Freedom, s.r.o. All rights reserved.
//

import SwiftUI
import PhotosUI

public struct AddVideoButton: View {
	@Binding var newVideo: PhotosPickerItem?
	
	let size: CGFloat = 40
	
	public init(newVideo: Binding<PhotosPickerItem?>) {
		self._newVideo = newVideo
	}
	
	public var body: some View {
		PhotosPicker(
			selection: $newVideo,
			matching: .videos,
			photoLibrary: .shared()
		) {
			Icon.plus
				.size(25)
				.padding(20)
				.foregroundColor(.black)
				.background(Color(hexString: "444444"))
				.clipShape(Circle())
				.frame(width: size, height: size)
		}
    }
}

#Preview {
	AddVideoButton(newVideo: .constant(nil))
		.padding(400)
		.background(Color(hexString: "222222"))
}
