//
//  Thumbnail.swift
//  FramesCore
//
//  Created by Martin Svoboda on 07.04.2024.
//  Copyright © 2024 Freedom, s.r.o. All rights reserved.
//

import SwiftUI

public struct Thumbnail: View {
	let uiImage: UIImage
	let size: CGSize
	let action: () -> Void
	
	public init(
		uiImage: UIImage,
		size: CGSize,
		action: @escaping () -> Void
	) {
		self.uiImage = uiImage
		self.size = size
		self.action = action
	}
	
	public var body: some View {
		Image(uiImage: uiImage)
			.resizable()
			.scaledToFill()
			.frame(width: size.width, height: size.height)
			.clipped()
			.overlay(alignment: .topTrailing) {
				Button(action: action) {
					Icon.xmark
						.size(20)
						.foregroundColor(.basicBlack.opacity(0.5))
						.padding(8)
				}
			}
    }
}

#Preview {
	Thumbnail(
		uiImage: UIImage(),
		size: .init(width: 300, height: 300),
		action: { }
	)
	.backgroundColor(.basicBlue.opacity(0.3))
}
