//
//  SheetView.swift
//  Frames
//
//  Created by Martin Svoboda on 13.02.2024.
//  Copyright © 2024 Freedom, s.r.o. All rights reserved.
//

import SwiftUI

public struct SheetView<Content: View>: View {
	@Environment(\.dismiss) private var dismiss
	@ObservedObject var router: Router
	
	let content: () -> Content
	
	public init(
		router: Router,
		content: @escaping () -> Content
	) {
		self.router = router
		self.content = content
	}

	public var body: some View {
		NavigationStack(path: $router.sheetPath) {
			content()
				.toolbar {
					Button(action: dismiss.callAsFunction) {
						Image(systemName: "xmark.circle.fill")
							.foregroundColor(.foregroundsSecondary)
					}
				}
		}
	}
}

#if DEBUG
#Preview {
	SheetView(router: Router.default) {
		ScreenView(title: "Změnit heslo") {
			VStack(alignment: .leading, spacing: 16) {
				Text(
					"""
					Pro změnu hesla vám z adresy help@template.cz zašleme odkaz pro potvrzení žádosti o změnu na váš e-mail.
					Platnost odkazu je 15 minut.
					"""
				)
				.textMedium()
				.foregroundColor(.foregroundsSecondary)
				.frame(maxWidth: .infinity, alignment: .leading)
			}
		}
	}
}
#endif
