//
//  Router.swift
//  FramesUI
//
//  Created by Martin Svoboda on 13.02.2024.
//  Copyright © 2024 Freedom, s.r.o. All rights reserved.
//

import SwiftUI
import Combine

public final class Router: ObservableObject {
	public static let `default` = Router()
	
	@Published public var toastItem: ToastItem?
	@Published public var sheetItem: SheetRoute?
	
	@Published public var examplePath: [ExampleRoute] = []
	@Published public var sheetPath: [SheetRoute] = []
	
	private var cancellables = Set<AnyCancellable>()
	
	// MARK: Init
	
	init() {
		setupBindings()
	}
	
	// MARK: Internal API
	
	func navigateTo(route: ExampleRoute) {
		examplePath.append(route)
	}
	
	func navigateTo(route: SheetRoute) {
		sheetPath.append(route)
	}
	
	func presentSheet(item: SheetRoute) {
		sheetItem = item
	}
	
	func presentToast(item: ToastItem, duration: TimeInterval) {
		withAnimation {
			toastItem = item
		}
		
		DispatchQueue.main.asyncAfter(deadline: .now() + duration) {
			withAnimation { [weak self] in
				self?.toastItem = nil
			}
		}
	}
	
	// MARK: Private API
	
	private func setupBindings() {
		$sheetItem
			.sink { [weak self] item in
				if item == nil {
					self?.sheetPath = []
				}
			}
			.store(in: &cancellables)
	}
}

public extension View {
	func navigateTo(route: ExampleRoute) {
		router.navigateTo(route: route)
	}
	
	func navigateTo(route: SheetRoute) {
		router.navigateTo(route: route)
	}
	
	func presentSheet(item: SheetRoute) {
		router.presentSheet(item: item)
	}
	
	func presentToast(
		text: String,
		icon: Image? = nil,
		iconColor: ColorItem? = nil,
		duration: TimeInterval = 3.0
	) {
		let item = ToastItem(
			text: text,
			icon: icon,
			iconColor: iconColor
		)
		router.presentToast(item: item, duration: duration)
	}
}

fileprivate extension View {
	var router: Router { .default }
}
