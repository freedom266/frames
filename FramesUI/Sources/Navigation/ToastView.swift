//
//  ToastView.swift
//  FramesUI
//
//  Created by Martin Svoboda on 13.02.2024.
//  Copyright © 2024 Freedom, s.r.o. All rights reserved.
//

import SwiftUI

public struct ToastItem {
	let text: String
	let icon: Image?
	let iconColor: ColorItem?
}

struct ToastView: View {
	let item: ToastItem
	
	var body: some View {
		HStack(spacing: 12) {
			if let icon = item.icon, let color = item.iconColor {
				icon
					.foregroundColor(color)
			}
			
			Text(item.text)
				.textMedium()
				.foregroundColor(.foregroundsPrimary)
				.lineLimit(1)
		}
		.padding(16)
		.backgroundColor(.backgroundsSecondary)
		.cornerRadius(30)
		.zIndex(1)
		.shadow(color: .black.opacity(0.16), radius: 6, x: 0, y: 2)
		.transition(.move(edge: .top))
		.padding(.bottom, 50) // for pretty animation
	}
}

#Preview {
	VStack(spacing: 40) {
		ToastView(
			item: .init(
				text: "Všechno je v pořádku",
				icon: Image(systemName: "checkmark.circle.fill"),
				iconColor: .basicGreen
			)
		)
		
		ToastView(
			item: .init(
				text: "Zkopírováno do schránky",
				icon: Image(systemName: "doc.on.doc"),
				iconColor: .basicBlue
			)
		)
	}
	.padding()
	.backgroundColor(.backgroundsPrimary)
}
