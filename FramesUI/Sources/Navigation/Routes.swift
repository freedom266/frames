//
//  Routes.swift
//  FramesUI
//
//  Created by Martin Svoboda on 13.02.2024.
//  Copyright © 2024 Freedom, s.r.o. All rights reserved.
//

import Foundation
import FramesCore

public enum ExampleRoute: Hashable {
	case exampleList
	case exampleDetail(Example.ID)
}

public enum SheetRoute: Identifiable {
	case exampleSheet
	
	public var id: SheetRoute {
		self
	}
}
