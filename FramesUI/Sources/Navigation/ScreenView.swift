//
//  ScreenView.swift
//  Frames
//
//  Created by Martin Svoboda on 13.02.2024.
//  Copyright © 2024 Freedom, s.r.o. All rights reserved.
//

import SwiftUI

public struct ScreenView<Content: View>: View {
	let title: String?
	let padding: CGFloat
	let content: () -> Content
	
	public init(
		title: String? = nil,
		horizontalPadding: CGFloat = 16,
		content: @escaping () -> Content
	) {
		self.title = title
		self.padding = horizontalPadding
		self.content = content
	}
	
	public var body: some View {
		ScrollView {
			Group {
				if let title {
					content()
						.navigationTitle(title)
				} else {
					content()
				}
			}
			.padding(.horizontal, padding)
		}
		.frame(maxWidth: .infinity)
		.backgroundColor(.backgroundsPrimary)
	}
}

#if DEBUG
#Preview {
	ScreenView(title: "Title") {
		VStack(spacing: 16) {
			Text("Lorem ipsum")
				.textMedium()
				.foregroundColor(.foregroundsSecondary)
				.frame(maxWidth: .infinity)
				.padding(.vertical, 40)
				.backgroundColor(.backgroundsSecondary)
				.cornerRadius(8)
			
			Text("Hello World")
				.textLarge()
				.foregroundColor(.foregroundsPrimary)
				.frame(maxWidth: .infinity)
				.padding(.vertical, 40)
				.backgroundColor(.backgroundsSecondary)
				.cornerRadius(8)
		}
	}
	.withNavigation()
}
#endif
