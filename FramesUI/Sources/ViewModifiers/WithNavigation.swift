//
//  WithNavigation.swift
//  Frames
//
//  Created by Martin Svoboda on 13.02.2024.
//  Copyright © 2024 Freedom, s.r.o. All rights reserved.
//

import SwiftUI

struct WithNavigationModifier: ViewModifier {
	func body(content: Content) -> some View {
		NavigationStack {
			content
		}
	}
}

/// Use it only for #Preview and snapshot testing
public extension View {
	func withNavigation() -> some View {
		modifier(WithNavigationModifier())
	}
}
