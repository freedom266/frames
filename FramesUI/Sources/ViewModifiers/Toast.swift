//
//  Toast.swift
//  FramesUI
//
//  Created by Martin Svoboda on 13.02.2024.
//  Copyright © 2024 Freedom, s.r.o. All rights reserved.
//

import SwiftUI

struct ToastModifier: ViewModifier {
	@Binding var item: ToastItem?

	func body(content: Content) -> some View {
		content
			.overlay(alignment: .top) {
				if let item {
					ToastView(item: item)
				}
			}
	}
}

public extension View {
	func toast(item: Binding<ToastItem?>) -> some View {
		modifier(ToastModifier(item: item))
	}
}
