//
//  Scrollable.swift
//  Frames
//
//  Created by Martin Svoboda on 13.02.2024.
//  Copyright © 2024 Freedom, s.r.o. All rights reserved.
//

import SwiftUI

struct ScrollableModifier: ViewModifier {
	func body(content: Content) -> some View {
		ScrollView {
			content
		}
	}
}

public extension View {
	func scrollable() -> some View {
		modifier(ScrollableModifier())
	}
}
