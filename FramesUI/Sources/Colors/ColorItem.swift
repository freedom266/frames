//
//  ColorItem.swift
//  Frames
//
//  Created by Martin Svoboda on 13.02.2024.
//  Copyright © 2024 Freedom, s.r.o. All rights reserved.
//

import SwiftUI

public struct ColorItem {
	private let hex: String
	private let alpha: Double
	
	init(hex: String, alpha: Double = 1) {
		self.hex = hex
		self.alpha = alpha
	}
	
	public var uiColor: UIColor {
		UIColor(hexString: hex).withAlphaComponent(alpha)
	}
	
	public var color: Color {
		Color(hexString: hex).opacity(alpha)
	}
	
	public func opacity(_ opacity: Double) -> ColorItem {
		.init(hex: hex, alpha: opacity)
	}
}

extension ColorItem: ExpressibleByStringLiteral {
	public init(stringLiteral value: StaticString) {
		self.hex = "\(value)"
		self.alpha = 1
	}
}

public extension ColorItem {
	
	// MARK: System colors
	
	/// #111111
	static var accentPrimary: ColorItem = "111111"
	
	/// #000000
	static var foregroundsPrimary: ColorItem = "000000"
	/// #666A80
	static var foregroundsSecondary: ColorItem = "666A80"
	/// #FFFFFF
	static var foregroundsOnPrimary: ColorItem = "FFFFFF"
	
	/// #F2F4F7
	static var backgroundsPrimary: ColorItem = "F2F4F7"
	/// #FFFFFF
	static var backgroundsSecondary: ColorItem = "FFFFFF"
	
	// MARK: Basic colors
	
	/// #FFFFFF
	static var basicWhite: ColorItem = "FFFFFF"
	/// #000000
	static var basicBlack: ColorItem = "000000"
	
	/// #1878F5
	static var basicBlue: ColorItem = "1878F5"
	/// #FF3B30
	static var basicRed: ColorItem = "FF3B30"
	/// #34C759
	static var basicGreen: ColorItem = "34C759"
}

public extension View {
	func foregroundStyle(_ primaryItem: ColorItem, _ secondaryItem: ColorItem) -> some View {
		foregroundStyle(primaryItem.color, secondaryItem.color)
	}
	
	func foregroundColor(_ colorItem: ColorItem) -> some View {
		foregroundStyle(colorItem.color)
	}

	func backgroundColor(_ colorItem: ColorItem) -> some View {
		background(colorItem.color)
	}
	
	func tint(_ colorItem: ColorItem) -> some View {
		tint(colorItem.color)
	}
}

public extension Shape {
	func fill(_ color: ColorItem, style: FillStyle = FillStyle()) -> some View {
		fill(color.color, style: style)
	}

	func stroke(_ color: ColorItem, style: StrokeStyle) -> some View {
		stroke(color.color, style: style)
	}

	func stroke(_ color: ColorItem, lineWidth: CGFloat = 1) -> some View {
		stroke(color.color, lineWidth: lineWidth)
	}
}

#if DEBUG
struct ColorItems_Previews: PreviewProvider {
	static let gridColumns = Array(repeating: GridItem(.flexible(), spacing: 0), count: 2)

	static var previews: some View {
		LazyVGrid(columns: gridColumns, spacing: 10) {
			color(title: "foregroundsPrimary", color: .foregroundsPrimary, textColor: .white)
			color(title: "foregroundsSecondary", color: .foregroundsSecondary)
			color(title: "foregroundsOnPrimary", color: .foregroundsOnPrimary, stroke: true)
			color(stroke: false)

			divider()
			
			color(title: "backgroundsPrimary", color: .backgroundsPrimary, stroke: true)
			color(title: "backgroundsSecondary", color: .backgroundsSecondary, stroke: true)
			
			divider()
			
			color(title: "basicBlue", color: .basicBlue)
			color(title: "basicRed", color: .basicRed)
			color(title: "basicGreen", color: .basicGreen)
			color(stroke: false)
		}
		.padding()
		.scrollable()
	}

	static func color(
		title: String = "",
		color: ColorItem = "FFFFFF",
		textColor: Color = .black,
		stroke: Bool = true
	) -> some View {
		RoundedRectangle(cornerRadius: 5)
			.fill(color)
			.overlay(alignment: .topLeading) {
				Text(title)
					.font(.system(size: 11, weight: .bold))
					.foregroundColor(textColor)
					.padding(8)
			}
			.frame(maxWidth: .infinity, maxHeight: .infinity)
			.aspectRatio(2, contentMode: .fit)
			.background {
				if stroke {
					RoundedRectangle(cornerRadius: 5)
						.stroke(.black, lineWidth: 3)
				}
			}
			.padding(.horizontal, 5)
	}
	
	static func divider() -> some View {
		Group {
			Divider().padding(.vertical)
			Divider().padding(.vertical)
		}
	}
}
#endif
