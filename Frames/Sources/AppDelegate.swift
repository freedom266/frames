import SwiftUI
import FramesCore
import SwiftyBeaver

@main
final class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?

    func application(
        _ application: UIApplication,
        didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]? = nil
    ) -> Bool {

		setupRootViewController()
		setupLogger()
		
        return true
    }
}

private extension AppDelegate {
	func setupRootViewController() {
		let window = UIWindow(frame: UIScreen.main.bounds)
		window.rootViewController = UIHostingController(rootView: AppView(viewModel: AppViewModel()))
		window.makeKeyAndVisible()
		self.window = window
	}
}

private extension AppDelegate {
	func setupLogger() {
		let consoleDestination = ConsoleDestination()
		consoleDestination.format = "$DHH:mm:ss.SSS$d ~ $C$L$c $N.$F:$l - $M $X"
		consoleDestination.levelColor.verbose = "📝 "
		consoleDestination.levelColor.debug = "🐛 "
		consoleDestination.levelColor.info = "ℹ️ "
		consoleDestination.levelColor.warning = "⚠️ "
		consoleDestination.levelColor.error = "❌ "
		#if DEBUG
			consoleDestination.minLevel = .verbose
		#else
			consoleDestination.minLevel = .warning
		#endif
		Logger.addDestination(consoleDestination)
	}
}
