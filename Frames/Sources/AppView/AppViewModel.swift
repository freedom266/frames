//
//  AppViewModel.swift
//  Frames
//
//  Created by Martin Svoboda on 13.02.2024.
//  Copyright © 2024 Freedom, s.r.o. All rights reserved.
//

import Combine
import FramesCore

protocol AppViewModeling: ObservableObject {
	var appState: AppState? { get }
}

final class AppViewModel: BaseViewModel, AppViewModeling {
	@Published var appState: AppState? = .edit

	// MARK: Init
	
	override init() {
		super.init()
		setupBindings()
	}
	
	// MARK: Private API
	
	private func setupBindings() {

	}
}
