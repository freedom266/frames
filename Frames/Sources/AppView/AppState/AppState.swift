//
//  AppState.swift
//  Frames
//
//  Created by Martin Svoboda on 13.02.2024.
//  Copyright © 2024 Freedom, s.r.o. All rights reserved.
//

import Foundation

enum AppState: String {
	case tabBar
	case edit
}
