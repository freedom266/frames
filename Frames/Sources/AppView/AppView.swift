//
//  AppView.swift
//  Frames
//
//  Created by Martin Svoboda on 13.02.2024.
//  Copyright © 2024 Freedom, s.r.o. All rights reserved.
//

import SwiftUI
import Edit
import TabBar

struct AppView<ViewModel: AppViewModeling>: View {
	@ObservedObject var viewModel: ViewModel
	
	private let tabBarViewModel = TabBarViewModel()
	
	var body: some View {
		switch viewModel.appState {
		case .tabBar:
			TabBarView(viewModel: tabBarViewModel)
		case .edit:
			EditView(viewModel: editVM())
		case .none:
			EmptyView()
		}
	}
}
