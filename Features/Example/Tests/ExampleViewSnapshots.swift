//
//  ExampleViewSnapshots.swift
//  Frames
//
//  Created by Martin Svoboda on 13.02.2024.
//  Copyright © 2024 Freedom, s.r.o. All rights reserved.
//

import XCTest
import FramesTesting
@testable import Example

class ExampleViewSnapshots: XCTestCase {
	func testPreviews() {
		let view = ExampleView(viewModel: ExampleViewModelMock()).withNavigation()
		AssertSnapshot(view)
	}
}
