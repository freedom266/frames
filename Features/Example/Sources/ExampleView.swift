//
//  ExampleView.swift
//  Example
//
//  Created by Martin Svoboda on 29.06.2023.
//  Copyright © 2023 Ackee, s.r.o. All rights reserved.
//

import SwiftUI
import FramesUI

public struct ExampleView<ViewModel: ExampleViewModeling>: View {
    @ObservedObject var viewModel: ViewModel
	@ObservedObject var router: Router
	
	public init(viewModel: ViewModel, router: Router) {
		self.viewModel = viewModel
		self.router = router
	}

    public var body: some View {
		NavigationStack(path: $router.examplePath) {
			ScreenView(title: "Example") {
				VStack(spacing: 20) {
					Button("Navigate to exampleList") {
						navigateTo(route: .exampleList)
					}
					.frame(maxWidth: .infinity)
					.padding(.vertical, 40)
					.backgroundColor(.backgroundsSecondary)
					.cornerRadius(8)
					
					Button("Present exampleSheet") {
						presentSheet(item: .exampleSheet)
					}
					.frame(maxWidth: .infinity)
					.padding(.vertical, 40)
					.backgroundColor(.backgroundsSecondary)
					.cornerRadius(8)
				}
			}
			.navigationDestination(for: ExampleRoute.self) { screen in
				switch screen {
				case .exampleList:
					EmptyView()
				case .exampleDetail:
					EmptyView()
				}
			}
		}
    }
}

#if DEBUG
#Preview {
	ExampleView(viewModel: ExampleViewModelMock(), router: Router.default)
		.withNavigation()
}
#endif
