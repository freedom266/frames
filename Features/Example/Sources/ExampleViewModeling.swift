//
//  ExampleViewModeling.swift
//  Example
//
//  Created by Martin Svoboda on 30.06.2023.
//  Copyright © 2023 Ackee, s.r.o. All rights reserved.
//

import Combine
import FramesCore

public protocol ExampleViewModeling: ObservableObject {

}

public func exampleVM() -> some ExampleViewModeling {
	ExampleViewModel(dependencies: dependencies.example)
}

final class ExampleViewModel: BaseViewModel, ExampleViewModeling {

    private let dependencies: ExampleDependencies

    // MARK: Init

    init(dependencies: ExampleDependencies) {
        self.dependencies = dependencies
    }
}
