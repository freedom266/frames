//
//  Tab.swift
//  Frames
//
//  Created by Martin Svoboda on 13.02.2024.
//  Copyright © 2024 Freedom, s.r.o. All rights reserved.
//

import SwiftUI

public enum Tab {
	case example
	
	var title: String {
		switch self {
		case .example: "Example"
		}
	}
	
	var icon: Image {
		switch self {
		case .example: Image(systemName: "apple.logo")
		}
	}
}

extension View {
	func tab(_ tab: Tab) -> some View {
		tag(tab)
			.tabItem {
				Label {
					Text(tab.title)
				} icon: {
					tab.icon
				}
			}
	}
}
