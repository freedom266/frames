//
//  TabBarView.swift
//  Frames
//
//  Created by Martin Svoboda on 13.02.2024.
//  Copyright © 2024 Freedom, s.r.o. All rights reserved.
//

import SwiftUI
import FramesUI
import Example

public struct TabBarView<ViewModel: TabBarViewModel>: View {
	@ObservedObject var viewModel: ViewModel
	@StateObject var router = Router.default
	
	public init(viewModel: ViewModel) {
		self.viewModel = viewModel
		setupTabBarAppearance()
	}
	
	public var body: some View {
		TabView(selection: tabSelection()) {
			ExampleView(viewModel: viewModel.example, router: router)
				.tab(Tab.example)
		}
		.toast(item: $router.toastItem)
		.sheet(item: $router.sheetItem) { route in
			SheetView(router: router) {
				sheetView(route: route)
					.navigationDestination(for: SheetRoute.self) { route in
						sheetView(route: route)
					}
			}
		}
	}
	
	private func tabSelection() -> Binding<Tab> {
		Binding(
			get: { viewModel.selectedTab },
			set: { newValue in
				/// User tapped on the tab twice --> pop to root view
				if viewModel.selectedTab == newValue {
					switch newValue {
					case .example:
						router.examplePath = []
					}
				}
				viewModel.selectedTab = newValue
			}
		)
	}
	
	@ViewBuilder func sheetView(route: SheetRoute) -> some View {
		switch route {
		case .exampleSheet:
			EmptyView()
		}
	}
}

private extension TabBarView {
	func setupTabBarAppearance() {
		let normalColor = ColorItem.foregroundsSecondary.uiColor
		let selectedColor = ColorItem.basicBlue.uiColor
		let backgroundColor = ColorItem.backgroundsSecondary.uiColor
		
		let normalAttributes: [NSAttributedString.Key: Any] = [.foregroundColor: normalColor]
		let selectedAttributes: [NSAttributedString.Key: Any] = [.foregroundColor: selectedColor]
		
		let itemAppearance = UITabBarItemAppearance()
		itemAppearance.normal.titleTextAttributes = normalAttributes
		itemAppearance.normal.iconColor = normalColor
		itemAppearance.selected.titleTextAttributes = selectedAttributes
		itemAppearance.selected.iconColor = selectedColor
		
		let appearance = UITabBarAppearance()
		appearance.configureWithOpaqueBackground()
		appearance.backgroundColor = backgroundColor
		appearance.inlineLayoutAppearance = itemAppearance
		appearance.stackedLayoutAppearance = itemAppearance
		appearance.compactInlineLayoutAppearance = itemAppearance
		
		UITabBar.appearance().standardAppearance = appearance
		UITabBar.appearance().scrollEdgeAppearance = appearance
	}
}

#if DEBUG
#Preview {
	TabBarView(viewModel: TabBarViewModel(selectedTab: .example))
}
#endif
