//
//  TabBarViewModel.swift
//  Frames
//
//  Created by Martin Svoboda on 13.02.2024.
//  Copyright © 2024 Freedom, s.r.o. All rights reserved.
//

import Combine
import FramesCore
import Example

public final class TabBarViewModel: BaseViewModel, ObservableObject {
	@Published var selectedTab: Tab
	
	lazy var example: some ExampleViewModeling = {
		exampleVM()
	}()
	
	// MARK: Init
	
	public init(selectedTab: Tab = .example) {
		self.selectedTab = selectedTab
		super.init()
	}
}
