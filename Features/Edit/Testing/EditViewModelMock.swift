//
//  EditViewModelMock.swift
//  Edit
//
//  Created by Martin Svoboda on 06.04.2024.
//  Copyright © 2024 Freedom, s.r.o. All rights reserved.
//

import SwiftUI
import PhotosUI
import Combine

final class EditViewModelMock: EditViewModeling {
	@Published var newTopVideo: PhotosPickerItem?
	@Published var newBottomVideo: PhotosPickerItem?
	@Published var topThumbnail: UIImage?
	@Published var bottomThumbnail: UIImage?
	@Published var isTopThumbnailLoading = true
	@Published var isBottomThumbnailLoading = false
	
	@Published var measuredSize: CGSize = .zero
	@Published var prevSeparatorOffset: CGFloat = 0
	@Published var currSeparatorOffset: CGFloat = 0
	
	var framesArea: CGSize {
		.init(
			width: measuredSize.width,
			height: measuredSize.width * 16 / 9
		)
	}
	
	var topRectangleSize: CGSize {
		.init(
			width: framesArea.width,
			height: (framesArea.height - separatorHeight) / 2 + currSeparatorOffset
		)
	}
	
	var bottomRectangleSize: CGSize {
		.init(
			width: framesArea.width,
			height: (framesArea.height - separatorHeight) / 2 - currSeparatorOffset
		)
	}
	
	var addButtonSize: CGSize {
		.init(
			width: measuredSize.width / 4,
			height: measuredSize.width / 4
		)
	}
	
	let separatorHeight: CGFloat = 20
	
	// MARK: Public API
	
	func updateCurrSeparatorOffset(newValue: CGFloat) {
		currSeparatorOffset = prevSeparatorOffset + newValue
	}
	
	func updatePrevSeparatorOffset(newValue: CGFloat) {
		prevSeparatorOffset += newValue
	}
	
	func removeTopVideo() { }
	func removeBottomVideo() { }
}
