//
//  EditView.swift
//  Edit
//
//  Created by Martin Svoboda on 06.04.2024.
//  Copyright © 2024 Freedom, s.r.o. All rights reserved.
//

import SwiftUI
import PhotosUI
import FramesUI
import FramesResources

public struct EditView<ViewModel: EditViewModeling>: View {
	@ObservedObject var viewModel: ViewModel
	
	public init(viewModel: ViewModel) {
		self.viewModel = viewModel
	}
	
	public var body: some View {
		GeometryReader { proxy in
			VStack(spacing: 0) {
				Spacer()
				
				VStack(spacing: viewModel.separatorHeight) {
					frameArea(size: viewModel.topRectangleSize)
						.overlay {
							if let thumbnail = viewModel.topThumbnail {
								Thumbnail(
									uiImage: thumbnail,
									size: viewModel.topRectangleSize,
									action: viewModel.removeTopVideo
								)
							} else if viewModel.isTopThumbnailLoading {
								ProgressView()
									.tint(.basicWhite)
							} else {
								AddVideoButton(newVideo: $viewModel.newTopVideo)
							}
						}
					
					frameArea(size: viewModel.bottomRectangleSize)
						.overlay {
							if let thumbnail = viewModel.bottomThumbnail {
								Thumbnail(
									uiImage: thumbnail,
									size: viewModel.bottomRectangleSize,
									action: viewModel.removeBottomVideo
								)
							} else if viewModel.isBottomThumbnailLoading {
								ProgressView()
									.tint(.basicWhite)
							} else {
								AddVideoButton(newVideo: $viewModel.newBottomVideo)
							}
						}
					
				}
				.frame(
					width: viewModel.framesArea.width,
					height: viewModel.framesArea.height
				)
				.background(Color(hexString: "333333"))
				.overlay {
					separator
				}
				
				Spacer()
			}
			.onAppear {
				viewModel.measuredSize = proxy.size
			}
		}
		.background(Color(hexString: "121212"))
	}
	
	private var separator: some View {
		Text("@martin_freedom")
			.labelMini()
			.frame(
				width: viewModel.framesArea.width,
				height: viewModel.separatorHeight
			)
			.foregroundColor(Color(hexString: "F7CE46"))
			.background(Color.black)
			.offset(y: viewModel.currSeparatorOffset)
			.gesture(
				DragGesture(minimumDistance: 0)
					.onChanged {
						viewModel.updateCurrSeparatorOffset(newValue: $0.translation.height)
					}
					.onEnded {
						viewModel.updatePrevSeparatorOffset(newValue: $0.translation.height)
					}
			)
	}
	
	private func frameArea(size: CGSize) -> some View {
		Rectangle()
			.fill(Color(hexString: "222222"))
			.frame(width: size.width, height: size.height)
	}
}

#Preview {
	EditView(viewModel: EditViewModelMock())
}
