//
//  EditViewModel.swift
//  Edit
//
//  Created by Martin Svoboda on 06.04.2024.
//  Copyright © 2024 Freedom, s.r.o. All rights reserved.
//

import SwiftUI
import PhotosUI
import Combine
import QuickLook
import FramesCore

public protocol EditViewModeling: ObservableObject {
	var newTopVideo: PhotosPickerItem? { get set }
	var newBottomVideo: PhotosPickerItem? { get set }
	var topThumbnail: UIImage? { get }
	var bottomThumbnail: UIImage? { get }
	var isTopThumbnailLoading: Bool { get }
	var isBottomThumbnailLoading: Bool { get }
	
	var measuredSize: CGSize { get set }
	var framesArea: CGSize { get }
	var separatorHeight: CGFloat { get }
	var currSeparatorOffset: CGFloat { get }
	var topRectangleSize: CGSize { get }
	var bottomRectangleSize: CGSize { get }
	var addButtonSize: CGSize { get }
	
	func updateCurrSeparatorOffset(newValue: CGFloat)
	func updatePrevSeparatorOffset(newValue: CGFloat)
	
	func removeTopVideo()
	func removeBottomVideo()
}

public func editVM() -> some EditViewModeling {
	EditViewModel()
}

final class EditViewModel: BaseViewModel, EditViewModeling {
	@Published var newTopVideo: PhotosPickerItem?
	@Published var newBottomVideo: PhotosPickerItem?
	@Published var topThumbnail: UIImage?
	@Published var bottomThumbnail: UIImage?
	
	var isTopThumbnailLoading: Bool {
		newTopVideo != nil && topThumbnail == nil
	}
	
	var isBottomThumbnailLoading: Bool {
		newBottomVideo != nil && bottomThumbnail == nil
	}
	
	@Published var measuredSize: CGSize = .init(width: 20, height: 20)
	@Published var prevSeparatorOffset: CGFloat = 0
	@Published var currSeparatorOffset: CGFloat = 0
	
	var framesArea: CGSize {
		.init(
			width: measuredSize.width,
			height: measuredSize.width * 16 / 9
		)
	}
	
	var maxSeparatorOffset: CGFloat {
		framesArea.height / 4
	}
	
	var topRectangleSize: CGSize {
		.init(
			width: framesArea.width,
			height: (framesArea.height - separatorHeight) / 2 + currSeparatorOffset
		)
	}
	
	var bottomRectangleSize: CGSize {
		.init(
			width: framesArea.width,
			height: (framesArea.height - separatorHeight) / 2 - currSeparatorOffset
		)
	}
	
	var addButtonSize: CGSize {
		.init(
			width: measuredSize.width / 4,
			height: measuredSize.width / 4
		)
	}
	
	let separatorHeight: CGFloat = 20
	
	public override init() {
		super.init()
		setupBindings()
	}
	
	// MARK: Public API
	
	func updateCurrSeparatorOffset(newValue: CGFloat) {
		currSeparatorOffset = safeOffset(newValue: newValue)
	}
	
	func updatePrevSeparatorOffset(newValue: CGFloat) {
		prevSeparatorOffset = safeOffset(newValue: newValue)
	}
	
	func removeTopVideo() {
		topThumbnail = nil
		newTopVideo = nil
	}
	
	func removeBottomVideo() {
		bottomThumbnail = nil
		newBottomVideo = nil
	}
	
	// MARK: Private API
	
	private func setupBindings() {
		$newTopVideo
			.dropFirst()
			.sink { newValue in
				Task { [weak self] in
					let thumbnail = await self?.thumbnail(newValue)
					DispatchQueue.main.async { [weak self] in
						self?.topThumbnail = thumbnail
					}
				}
			}
			.store(in: &cancellables)
		
		$newBottomVideo
			.dropFirst()
			.sink { newValue in
				Task(priority: .userInitiated) { [weak self] in
					let thumbnail = await self?.thumbnail(newValue)
					DispatchQueue.main.async { [weak self] in
						self?.bottomThumbnail = thumbnail
					}
				}
			}
			.store(in: &cancellables)
	}
	
	private func thumbnail(_ newVideo: PhotosPickerItem?) async -> UIImage? {
		guard 
			let data = await newVideo?.data(),
			let tempFile = TempMediaFile(data: data),
			let asset = tempFile.avAsset
		else { return nil }

		let thumbnail = await asset.generateThumbnail()
		
		Logger.info("Return thumbnail")
		return thumbnail
	}
	
	private func safeOffset(newValue: CGFloat) -> CGFloat {
		let offsetProposal = prevSeparatorOffset + newValue
		if abs(offsetProposal) > maxSeparatorOffset {
			return offsetProposal > 0 ? maxSeparatorOffset : -maxSeparatorOffset
		} else {
			return offsetProposal
		}
	}
}
