//
//  Aliases.swift
//  Frames
//
//  Created by Martin Svoboda on 06.08.2023.
//  Copyright © 2023 Freedom, s.r.o. All rights reserved.
//

public typealias Assets = FramesResourcesAsset
