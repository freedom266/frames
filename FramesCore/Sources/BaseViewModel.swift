//
//  BaseViewModel.swift
//  FramesCore
//
//  Created by Martin Svoboda on 13.02.2024.
//  Copyright © 2024 Freedom, s.r.o. All rights reserved.
//

import Combine
import ACKategories

open class BaseViewModel: Base.ViewModel {
	
	public var cancellables = Set<AnyCancellable>()
}
