//
//  String+Extensions.swift
//  FramesCore
//
//  Created by Martin Svoboda on 13.02.2024.
//  Copyright © 2024 Freedom, s.r.o. All rights reserved.
//

import SwiftUI

public extension String {
	func copyToClipboard() {
		UIPasteboard.general.string = self
	}
}
