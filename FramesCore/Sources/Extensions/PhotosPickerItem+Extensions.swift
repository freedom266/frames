//
//  PhotosPickerItem+Extensions.swift
//  FramesCore
//
//  Created by Martin Svoboda on 07.04.2024.
//  Copyright © 2024 Freedom, s.r.o. All rights reserved.
//

import SwiftUI
import PhotosUI

extension PhotosPickerItem {
	public func data() async -> Data? {
		do {
			Logger.info("loadTransferable")
			let data = try await loadTransferable(type: Data.self)
			return data
		} catch {
			Logger.error(error)
			return nil
		}
	}
}
