//
//  AVAsset+Extensions.swift
//  FramesCore
//
//  Created by Martin Svoboda on 07.04.2024.
//  Copyright © 2024 Freedom, s.r.o. All rights reserved.
//

import Foundation
import SwiftUI
import AVFoundation

extension AVAsset {
	public func generateThumbnail() async -> UIImage? {
		Logger.info("Generate thumbnail")
		
		let imageGenerator = AVAssetImageGenerator(asset: self)
		let time = CMTime(seconds: 0.0, preferredTimescale: 600)

		if let cgImage = try? await imageGenerator.image(at: time).image {
			return UIImage(cgImage: cgImage)
		}
		
		return nil
	}
}
