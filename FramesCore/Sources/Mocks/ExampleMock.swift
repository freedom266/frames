//
//  ExampleMock.swift
//  FramesCore
//
//  Created by Martin Svoboda on 13.02.2024.
//  Copyright © 2024 Freedom, s.r.o. All rights reserved.
//

import Foundation

public extension Example {
	static var mock1: Example {
		.init(id: "1")
	}
}
