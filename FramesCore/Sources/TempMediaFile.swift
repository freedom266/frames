//
//  TempMediaFile.swift
//  FramesCore
//
//  Created by Martin Svoboda on 07.04.2024.
//  Copyright © 2024 Freedom, s.r.o. All rights reserved.
//
// https://stackoverflow.com/questions/50012567/how-to-convert-data-to-avasset

import Foundation
import AVKit

public class TempMediaFile {
	var url: URL?
	
	public init?(data: Data?) {
		guard let data else { return nil }
		Logger.info("Init tempFile")
		
		let directory = FileManager.default.temporaryDirectory
		let fileName = "\(NSUUID().uuidString).mov"
		let url = directory.appendingPathComponent(fileName)
		
		do {
			try data.write(to: url)
			self.url = url
		} catch {
			Logger.error(error)
		}
	}
	
	public var avAsset: AVAsset? {
		if let url {
			Logger.info("Get avAsset")
			return AVAsset(url: url)
		}
		
		return nil
	}
	
	public func deleteFile() {
		if let url = self.url {
			do {
				try FileManager.default.removeItem(at: url)
				self.url = nil
			} catch {
				Logger.error(error)
			}
		}
	}
	
	deinit {
		self.deleteFile()
	}
}
